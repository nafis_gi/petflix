import { PrismaClient } from "@prisma/client";
import axios from 'axios';

const prisma = new PrismaClient();

const HEADERS = {
  'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8,ru-RU;q=0.7',
  'Cache-Control': 'no-cache',
  Connection: 'keep-alive',
  Origin: 'https://developer.themoviedb.org',
  Pragma: 'no-cache',
  Referer: 'https://developer.themoviedb.org/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'cross-site',
  'User-Agent':
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
  accept: 'application/json',
  authorization:
    'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmMTZjNWYzNDViODliY2U4ZmY1ODdlNDBjODA4NzY5NiIsInN1YiI6IjY0N2NlNjMzMjYzNDYyMDBiZjM2NDBkOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.F4J1HiYvrsvOROfEmek0qv-0XI5ISSfXUBVTe4Na5yk',
  'sec-ch-ua':
    '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'x-readme-api-explorer': '4.408.0',
};

type MovieDTO = {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
};

(async () => {
  for (let i = 3; i <= 500; i++) {
    try {
      const data = await axios
        .get<{ results: MovieDTO[] }>(
          `https://try.readme.io/https://api.themoviedb.org/3/movie/top_rated?language=ru-RU&page=${i}`,
          {
            headers: HEADERS,
          }
        )
        .then(({ data }) => data.results);

      for (const item of data) {
        await prisma.movie.create({
          data: {
            title: item.title,
            originalTitle: item.original_title,
            overview: item.overview,
            tmvdbId: item.id,
            releaseDate: item.release_date,
            rating: item.popularity,
            genreIds: item.genre_ids,
          },
        });
      }
      console.log('added', i);
    } catch (error) {
      console.error(error);
    }
  }
})();
