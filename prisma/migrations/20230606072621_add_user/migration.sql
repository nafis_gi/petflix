-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL,
    "watchedMovies" INTEGER[],

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);
