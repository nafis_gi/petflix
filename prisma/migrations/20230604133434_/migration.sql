/*
  Warnings:

  - You are about to drop the column `name` on the `Ihor` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Ihor" DROP COLUMN "name",
ADD COLUMN     "data" JSONB;
