import express from 'express';
import morgan from 'morgan';
import passport from "passport";
import * as dotenv from 'dotenv'; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import session from "express-session";
dotenv.config();

import bodyParser from 'body-parser';
import { extendContextWithDb } from './db';
import { routes } from './routes';
import { cookieParserMiddleware } from './middlewares';

const app = express();
const port = process.env.PORT;

app.set("view engine", "ejs");
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(cookieParserMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(extendContextWithDb);

app.use(routes);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
