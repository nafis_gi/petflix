import cookieParser from "cookie-parser";
import { COOKIE_SECRET } from "../config";

export const cookieParserMiddleware = cookieParser(COOKIE_SECRET);
