import { prisma, elasticsearch, driverNeo4j } from "../db";

// to make the file a module and avoid the TypeScript error
export {};

declare global {
  namespace Express {
    export interface Request {
      context: {
        db: typeof prisma;
        elasticsearch: typeof elasticsearch;
        driverNeo4j: typeof driverNeo4j;
      };
    }
    interface User {
      id: string;
      displayName: string;
      name: {
        familyName: string;
        givenName: string;
      };
      privileged?: boolean;
    }
  }
}
