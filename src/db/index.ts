import { PrismaClient } from "@prisma/client";
import { NextFunction, Request, Response } from "express";
import { elasticsearch } from './elastic';
import { driverNeo4j } from './neo4j';

const prisma = new PrismaClient();

const extendContextWithDb = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  req.context = req.context || {};
  req.context.db = prisma;
  req.context.elasticsearch = elasticsearch;
  req.context.driverNeo4j = driverNeo4j;

  next();
};
export { elasticsearch, prisma, driverNeo4j, extendContextWithDb };
