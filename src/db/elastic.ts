import { Client } from '@elastic/elasticsearch';

const elasticsearch = new Client({
  node: `http://localhost:${process.env.ES_PORT}`,
});

export { elasticsearch };
