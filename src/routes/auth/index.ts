import { Router } from "express";
import {
  googleAuthHandler,
  googleAuthCallbackHandler,
  logoutHandler,
} from "./auth.controller";
import { authMiddleware } from '../../middlewares';

const routes = Router();

routes.get('/login', authMiddleware, (req, res) => {
  res.send("Вы авторизованы");
});

routes.get('/google', googleAuthHandler);

routes.get('/google/callback', googleAuthCallbackHandler);

routes.get('/logout', logoutHandler);

export { routes };
