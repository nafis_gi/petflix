import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const scanPagesBody = z
  .object({
    scanPages: z.number().optional(),
  })
  .optional();

export const generateRecommendationController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const session = req.context.driverNeo4j.session();
  const db = req.context.db;

  try {
    const data = scanPagesBody.parse(req.body);
    const pages = data?.scanPages || 1;
    const maxPage = 500;
    for (let page = 1; page < maxPage && page <= pages; page += 1) {
      const movies = await db.movie.findMany({
        take: 100,
        skip: (page - 1) * 100,
      });
      if (!movies.length) throw "Nothing found";
      await session.run(
        `
  UNWIND $movies AS row
  MERGE (m:Movie {id: row.id})
  SET m.title = row.title, m.releaseDate = row.releaseDate, m.overview = row.overview
  WITH m, row.genreIds AS genreIds
  UNWIND genreIds AS genreId
  MERGE (g:Genre {id: genreId})
  MERGE (m)-[:IN_GENRE]->(g)
`,
        { movies }
      );
    }

    res.json({
      status: "ok",
    });
  } catch (err: any) {
    next(err);
  } finally {
    session.close();
  }
};
