import { NextFunction, Request, Response } from "express";

export const recommendationController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const session = req.context.driverNeo4j.session();
  const userId = req.user?.id;
  if (!userId) {
    throw 'User nor found';
  }

  try {
    // Запрос к базе данных для получения списка просмотренных фильмов пользователя
    const { records: watchedMoviesRecords } = await session.run(
      'MATCH (u:User)-[:WATCHED]->(m:Movie) WHERE u.id = $userId RETURN m.id AS movieId',
      { userId }
    );
    const watchedMoviesId = watchedMoviesRecords.map((record) =>
      record.get('movieId')
    );

    // Запрос к базе данных для получения списка жанров просмотренных фильмов
    const { records: genreRecords } = await session.run(
      'MATCH (m:Movie)-[:IN_GENRE]->(g:Genre) WHERE m.id IN $movieIds RETURN g.id AS genreId',
      { movieIds: watchedMoviesId }
    );
    const genreIds = genreRecords.map((record) => record.get('genreId'));

    // Запрос к базе данных для получения списка рекомендуемых фильмов
    const result = await session.run(
      'MATCH (m:Movie)-[:IN_GENRE]->(g:Genre) WHERE g.id IN $genres RETURN m.id AS movieId',
      { genres: genreIds }
    );
    const data = result.records.map((record) => record.get('movieId'));

    res.json({
      data,
    });
  } catch (error) {
    next(error);
  } finally {
    session.close();
  }
};
