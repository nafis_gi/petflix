import express from "express";
import { generateRecommendationController } from './reccomedation-generate-data.controller';
import { recommendationController } from './reccomendation.controller';
import { authMiddleware } from "../../middlewares";

const routes = express.Router();

routes.get('/', authMiddleware, recommendationController);
routes.post('/generate', generateRecommendationController);

export { routes };
