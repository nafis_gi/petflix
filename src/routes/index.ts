import express from 'express';
import { errorHandler } from './error.handler';
import { healthController } from './health.controller';
import { routes as authRoutes } from './auth';
import { routes as searchRoutes } from './search';
import { routes as watchRoutes } from './watch';
import { routes as recommendationsRoutes } from './reccomendation';
import { routes as downloadRoutes } from './download';
import { authMiddleware } from "../middlewares";

const routes = express.Router();

routes.use('/health', healthController);
routes.use('/auth', authRoutes);
routes.use('/search', authMiddleware, searchRoutes);
routes.use('/watch', watchRoutes);
routes.use('/recommendations', recommendationsRoutes);
routes.use('/download', authMiddleware, downloadRoutes);

routes.use(errorHandler);

export { routes };
