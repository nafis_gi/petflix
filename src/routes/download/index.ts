import express from "express";
import { downloadController } from './download.controller';

const routes = express.Router();

routes.get('/', downloadController);

export { routes };
