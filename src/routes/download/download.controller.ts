import { NextFunction, Request, Response } from "express";
import TorrentSearchApi from "torrent-search-api";
import { z } from "zod";

const querySchema = z.object({
  query: z.string().optional(),
  id: z
    .preprocess((a) => Number.parseInt(a as string, 10), z.number().positive())
    .optional(),
});

export const downloadController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  TorrentSearchApi.enableProvider('ThePirateBay');
  try {
    const { query, id } = querySchema.parse(req.query);

    if (id) {
      const movie = await req.context.db.movie.findFirst({
        where: {
          id,
        },
      });

      if (movie) {
        const torrents = await TorrentSearchApi.search(
          movie.originalTitle,
          'All',
          20
        );
        res.json({
          data: torrents,
        });
      }
    }

    if (query) {
      const torrents = await TorrentSearchApi.search(query, 'All', 20);
      res.json({
        status: torrents,
      });
    }

    res.json({
      status: 'not found',
    });
  } catch (error) {
    next(error);
  }
};
