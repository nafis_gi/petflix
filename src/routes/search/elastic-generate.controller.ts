import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const scanPagesBody = z
  .object({
    scanPages: z.number().optional(),
  })
  .optional();

export const generateElasticController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const elasticsearch = req.context.elasticsearch;
  const db = req.context.db;

  try {
    const data = scanPagesBody.parse(req.body);
    const pages = data?.scanPages || 1;
    const maxPage = 500;

    for (let page = 1; page < maxPage && page <= pages; page += 1) {
      const movies = await db.movie.findMany({
        take: 100,
        skip: (page - 1) * 100,
      });
      if (!movies.length) throw "Nothing found";

      for (const movie of movies) {
        const result = await elasticsearch.index({
          index: "movies",
          body: {
            title: movie.title,
            year: new Date(movie.releaseDate).getFullYear().toString(),
            overview: movie.overview,
            id: movie.id,
            tmvdbId: movie.tmvdbId,
          },
        });
      }
    }

    res.json({
      status: "ok",
    });
  } catch (err: any) {
    next(err);
  }
};
