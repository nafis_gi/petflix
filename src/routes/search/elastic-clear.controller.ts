import { NextFunction, Request, Response } from "express";

export const clearElasticController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const elasticsearch = req.context.elasticsearch;

  try {
    const result = await elasticsearch.indices.delete({ index: 'movies' });

    res.json({
      status: 'ok',
    });
  } catch (err: any) {
    next(err);
  }
};
