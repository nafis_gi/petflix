import type { NextFunction, Request, Response } from "express";
import { z } from 'zod';

const paginationSchema = z.object({
  page: z.preprocess(
    (a) => Number.parseInt(a as string, 10),
    z.number().positive()
  ),
  size: z.preprocess(
    (a) => Number.parseInt(a as string, 10),
    z.number().positive().max(100)
  ),
});

export const getAllController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { db } = req.context;
  try {
    const { size, page } = paginationSchema.parse(req.query);
    const data = await db.movie.findMany({
      skip: (page - 1) * size,
      take: size,
    });

    res.json({
      data,
    });
  } catch (err) {
    next(err);
  }
};
