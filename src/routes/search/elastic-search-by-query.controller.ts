import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const getBody = z.object({
  query: z.string(),
});

export const elasticSearchByQuery = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const elasticsearch = req.context.elasticsearch;

  try {
    const { query } = getBody.parse(req.query);

    const result = await elasticsearch.search({
      index: "movies",
      body: {
        query: {
          multi_match: {
            query,
            fields: ["title", "overview", "year"],
          },
        },
      },
    });

    res.json({
      data: result.hits.hits.map((hit) => hit._source),
    });
  } catch (err: any) {
    next(err);
  }
};
