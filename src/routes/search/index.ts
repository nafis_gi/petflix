import express from 'express';
import { getAllController } from './search.controller';
import { elasticSearchByQuery } from './elastic-search-by-query.controller';
import { generateElasticController } from './elastic-generate.controller';
import { clearElasticController } from './elastic-clear.controller';

const routes = express.Router();

routes.get('/get-all', getAllController);
routes.get('/elastic-search', elasticSearchByQuery);
routes.post('/generate-elastic', generateElasticController);
routes.post('/clear-elastic', clearElasticController);

export { routes };
