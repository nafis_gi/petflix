import express from "express";
import { watchController } from './watch.controller';
import { authMiddleware } from "../../middlewares";

const routes = express.Router();

routes.post('/watch', authMiddleware, watchController);

export { routes };
