import { NextFunction, Request, Response } from "express";
import { z } from 'zod';

const querySchema = z.object({
  id: z.preprocess(
    (a) => Number.parseInt(a as string, 10),
    z.number().positive()
  ),
});

export const checkIfExists = async ({
  session,
  query,
  params,
}: {
  session: any;
  query: string;
  params: any;
}) => {
  const data = await session.run(
    `MATCH (${query})
    WITH count(*) as count
    return count`,
    params
  );

  return !!data.records.find((e: any) => e._fieldLookup.count > 0);
};

export const watchController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const session = req.context.driverNeo4j.session();
  try {
    const { id } = querySchema.parse(req.query);

    const userId = req.user?.id;

    if (userId) {
      const isUserExists = await checkIfExists({
        session,
        query: "(u:User {id: $id})",
        params: { id: userId },
      });

      !isUserExists &&
        (await session.run("CREATE (u:User {id: $id})", { id: userId }));

      const isMovieExists = await checkIfExists({
        session,
        query: "(m:Movie {id: $id})",
        params: { id },
      });

      !isMovieExists &&
        (await session.run('CREATE (m:Movie {id: $id})', { id }));

      // Запрос к базе данных для создания связи между пользователем и фильмом
      await session.run(
        'MATCH (u:User), (m:Movie) WHERE u.id = $userId AND m.id = $movieId CREATE (u)-[:WATCHED]->(m)',
        { userId, movieId: id }
      );
      res.json({
        status: 'ok',
      });
    } else {
      throw new Error('Not found user id');
    }
  } catch (error) {
    next(error);
  } finally {
    session.close();
  }
};
